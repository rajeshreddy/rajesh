package healthPermium;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import healthPermium.BasicPremiumCalculation;
import healthPermium.GenderPremiumCal;
import healthPermium.HabitsPremiumCal;
import healthPermium.PreExistingHealthCondisitionCal;

public class HelthPremiumMainClass {

	public static void main(String[] args) {
		System.out.println("Emter your name: ");
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String name = sc.nextLine();
		
		System.out.println("Emter your gender: male or female or third gender ");
		String gender = sc.nextLine();
		
		System.out.println("Emter your age: ");
		int age = sc.nextInt();
		
		System.out.println("Emter your current helth conditions, enter yes or no");
		sc.nextLine();
		System.out.println("1. Hypertension "); 
		String hypo = sc.nextLine();
		
		System.out.println("2. Blood pressure "); 
		String bp = sc.nextLine();
		System.out.println("3. Blood sugar "); 
		String sugar = sc.nextLine();
		System.out.println("4. Overweight "); 
		String wt = sc.nextLine();
		
		Map<String, String> helth = new HashMap<>();
		helth.put("Hypertension", hypo);
		helth.put("Blood Pressure", bp);
		helth.put("Blood Sugar", sugar);
		helth.put("Overweight", wt);
		
		
		System.out.println("Emter your habits, enter yes or no");
		System.out.println("1. Smoking "); 
		String sm = sc.nextLine();
		
		System.out.println("2. Alcohol "); 
		String al = sc.nextLine();
		System.out.println("3. Daily exercise"); 
		String de = sc.nextLine();
		System.out.println("4. Drugs "); 
		String dr = sc.nextLine();
		
		Map<String, String> habit = new HashMap<>();
		habit.put("Smoking", sm);
		habit.put("Alcohol", al);
		habit.put("Daily exercise", de);
		habit.put("Drugs", dr);
		
		BasicPremiumCalculation basic = new BasicPremiumCalculation();
		double amt = basic.premiumCal(age);
		GenderPremiumCal genderCal = new GenderPremiumCal();
		amt = genderCal.getGenderPremiumAmt(gender, amt);
		PreExistingHealthCondisitionCal helthCal = new PreExistingHealthCondisitionCal();
		amt = helthCal.getHelthConditionAmt(helth, amt);
		HabitsPremiumCal habitCal = new HabitsPremiumCal();
		amt = habitCal.getHabitsConditionAmt(habit, amt);
		
		System.out.println("Health Insurance Premium for Mr. "+name + " Rs." + amt);
		
	}
}
