package healthPermium;

public class GenderPremiumCal {
	public double getGenderPremiumAmt(String gender, double amt) {
		double per = 2;
		double div = 100;
		if (gender.equalsIgnoreCase("male")) {
			double percentage = 0;
			percentage = ((double)per/(double) div)*amt;
			amt = amt + percentage;
		}
		
		return amt;
	}
}
