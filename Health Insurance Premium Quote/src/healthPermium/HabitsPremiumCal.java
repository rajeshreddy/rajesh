package healthPermium;

import java.util.Map;

public class HabitsPremiumCal {
	public double getHabitsConditionAmt(Map<String, String> habits, double amt) {
		double per = 3;
		double div = 100;
		double percentage = 0;
		int reduce = 0;
		int increse = 0;
		for (Map.Entry<String, String> map : habits.entrySet()) {
			if (map.getValue().equalsIgnoreCase("Yes")) {
				if (map.getKey().equalsIgnoreCase("Daily exercise")) {
					reduce++;
				} else {
					increse++;
				}
			}
		}
		if ((increse - reduce) > 0) {
			percentage = ((double)per/(double) div)*amt;
			percentage = percentage * (increse - reduce);
			amt = amt + percentage;
		}
		
		return amt;
	}
}
