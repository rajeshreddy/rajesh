package JunitTestCases;


import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import healthPermium.PreExistingHealthCondisitionCal;

class PreExistingHealthCondisitionCalJunit {

	@Test
	void test() {
		PreExistingHealthCondisitionCal preGelthCond = new PreExistingHealthCondisitionCal();
		Map<String, String> helth = new HashMap<>();
		helth.put("Hypertension", "no");
		helth.put("Blood Pressure", "yes");
		helth.put("Blood Sugar", "no");
		helth.put("Overweight", "no");
		
		double calAmt = preGelthCond.getHelthConditionAmt(helth, 6171.0);
		Assertions.assertEquals(1, preGelthCond.per);
		Assertions.assertEquals(100, preGelthCond.div);
		Assertions.assertEquals(6232.71, calAmt);
	}

}
