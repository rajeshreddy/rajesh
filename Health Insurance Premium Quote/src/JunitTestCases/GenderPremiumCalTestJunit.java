package JunitTestCases;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import healthPermium.GenderPremiumCal;

class GenderPremiumCalTestJunit {

	@Test
	void genderVal() {
		GenderPremiumCal  tester = new GenderPremiumCal();
		double premiumAmt = tester.getGenderPremiumAmt("Male", 6050.0);
		Assertions.assertEquals(6171.0, premiumAmt);
	}

}
