package JunitTestCases;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import healthPermium.BasicPremiumCalculation;

class BasicPremiumCalculationJunit {

	@Test
	void testPremiumCal() {
		BasicPremiumCalculation  tester = new BasicPremiumCalculation();
		double premiumAmt = tester.premiumCal(26);
		Assertions.assertEquals(6050.0, premiumAmt);
	}

}
